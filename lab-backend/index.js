const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
require('dotenv').config()
const {pool} = require('./config')
import userService from 'services/users';


const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(cors())


app
  .route('/users')
  // GET endpoint
  .get(getUsers)
  // POST endpoint
  .post(addUser)


app.use('/api', userService);
// Start server
app.listen(process.env.PORT, () => {
  console.log(`Server listening on ` + (process.env.PORT || 3002));
})