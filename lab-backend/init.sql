DROP TABLE IF EXISTS users;
CREATE TABLE users (
  ID serial,
  fullname VARCHAR(255) NOT NULL,
  account VARCHAR(255) NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO users (fullname, account)
VALUES  ('Trung Nguyen', 'TrungNV35');