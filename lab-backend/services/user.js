import express from 'express';
require('dotenv').config()
const {pool} = require('./config')
const router = express.Router();


const getUsers = (request, response) => {

    pool.query('SELECT * FROM users', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
      console.log(`Get result ` + results.rows[0]);
    })
  }
  
const addUser = (request, response) => {
    const {fullname, account} = request.body

    pool.query(
        'INSERT INTO books (fullname, account) VALUES ($1, $2)',
        [fullname, account],
        (error) => {
        if (error) {
            throw error
        }
        response.status(201).json({status: 'success', message: 'User added.'})
        },
    )
}

/** GET /api-status - Check service status **/
router.get('/api-status', (req, res) =>
  res.json({
    status: "ok"
  })
);

export default users;