import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
   staff: any = {fullname: 'Kuberneters'};

   constructor(private http: HttpClient) {
     this.http.get('http://lab-demo-lab-deploy-v2-backend:8080/users')
     .subscribe(response => {
         this.staff.fullname = response[0].fullname;
     });
   }
}
